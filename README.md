# ImgurImageUploader
An easy to understand Python script that let you upload images to Imgur from Linux Terminal

To run the script you should have installed the Imgur python client and pyperclip.

To install Imgur python client: `pip install imgurpython`

To install pyperclip: `pip install pyperclip`

Change the script permission by doing: `chmod +x iiu.py`

The last thing you need to do is place the script wherever you want and add this to .bashrc: `export PATH=$PATH:~/Path_Of_The_Script/`

Close and Restart the Terminal, go in the pic directory and write: `iiu.py image_name`

If you'd like an easier shell command like `iiu image_name` before making the script executable, remove the `.py` or change its name in whatever you want!
