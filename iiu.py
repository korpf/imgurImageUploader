#!/usr/bin/env python
#
#Install imgurpython and pyperclip
#imgurpython: pip install imgurpython
#pyperclip: pip install pyperclip
#change permission to the script: chmod +x iiu.py
#add to .bashrc: export PATH=$PATH:~/Path_Of_The_Script
#write iiu.py image_name to obtain the link of the uploaded image
#
from imgurpython import ImgurClient
import pyperclip
import sys, os

### Imgur Client
client_id = '14eb464fe2552c1'
client_secret = '256322e9bc339c6e686a8067ac0fe25308c40150'
client = ImgurClient(client_id, client_secret)

cwd = os.getcwd() #get current directory
pathName = cwd + "/" + sys.argv[1]

url_imgur = client.upload_from_path(pathName, config=None, anon=True)
pic = url_imgur["link"]
pyperclip.copy(pic)
print("The link has been copied to the clipboard")
